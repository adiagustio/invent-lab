<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Invent</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/fontastic.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/grasp_mobile_progress_circle-1.0.0.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/style.default.css');?>" id="theme-stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/custom.css');?>">
  <link rel="shortcut icon" href="<?php echo base_url('/assets/template/img/favicon.png');?>">
</head>
<body>
  <nav class="side-navbar">
    <div class="side-navbar-wrapper">
      <div class="sidenav-header d-flex align-items-center justify-content-center">
        <div class="sidenav-header-inner text-center"><img src="<?php echo base_url('/assets/template/img/xxx.jpg');?>" alt="person" class="img-fluid rounded-circle">
          <h2 class="h5">Adiguest</h2><span>Web Developer</span>
        </div>
        <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>B</strong><strong class="text-primary">D</strong></a></div>
      </div>
      <div class="main-menu">
        <h5 class="sidenav-heading">Main</h5>
        <ul id="side-main-menu" class="side-menu list-unstyled">                  
          <li class="active"><?php echo anchor('home', 'Beranda');?></li>
          <li><?php echo anchor('home/barang', 'Barang');?></li>
          <li><?php echo anchor('home/lab', 'Lab');?></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="page">
    <!-- navbar-->
    <header class="header">
      <nav class="navbar">
        <div class="container-fluid">
          <div class="navbar-holder d-flex align-items-center justify-content-between">
            <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
              <div class="brand-text d-none d-md-inline-block"><span>Menu </span><strong class="text-primary">Dashboard</strong></div></a></div>
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Log out-->
                <li class="nav-item"><a href="<?= site_url('login/logout')?>" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div align="center">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="<?php echo base_url('/assets/template/img/4.jpg');?>" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="<?php echo base_url('/assets/template/img/3.jpg');?>" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="<?php echo base_url('/assets/template/img/2.jpg');?>" class="d-block w-100" alt="...">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>Adiguest &copy; 2018</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- JavaScript files-->
    <script src="<?php echo base_url('/assets/template/vendor/jquery/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/vendor/popper.js/umd/popper.min.js');?>"> </script>
    <script src="<?php echo base_url('/assets/template/vendor/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/js/grasp_mobile_progress_circle-1.0.0.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/vendor/jquery.cookie/jquery.cookie.js')?>"> </script>
    <script src="<?php echo base_url('/assets/template/vendor/jquery-validation/jquery.validate.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js');?>"></script>
    <!-- Main File-->
    <script src="<?php echo base_url('/assets/template/js/front.js');?>"></script>
  </body>
  </html>