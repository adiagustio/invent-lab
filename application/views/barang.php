<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Invent</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/fontastic.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/grasp_mobile_progress_circle-1.0.0.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/style.default.css');?>" id="theme-stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/custom.css');?>">
  <link rel="shortcut icon" href="<?php echo base_url('/assets/template/img/favicon.png');?>">
</head>
<body>
  <!-- Side Navbar -->
  <nav class="side-navbar">
    <div class="side-navbar-wrapper">
      <!-- Sidebar Header    -->
      <div class="sidenav-header d-flex align-items-center justify-content-center">
        <!-- User Info-->
        <div class="sidenav-header-inner text-center"><img src="<?php echo base_url('/assets/template/img/xxx.jpg');?>" alt="person" class="img-fluid rounded-circle">
          <h2 class="h5">Adiguest</h2><span>Web Developer</span>
        </div>
        <!-- Small Brand information, appears on minimized sidebar-->
        <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>U</strong><strong class="text-primary">PT</strong></a></div>
      </div>
      <!-- Sidebar Navigation Menus-->
      <div class="main-menu">
        <h5 class="sidenav-heading">Main</h5>
        <ul id="side-main-menu" class="side-menu list-unstyled">  
          <li><?php echo anchor('home', 'Beranda');?></li>
          <li class="active"><?php echo anchor('home/barang', 'Barang');?></li>
          <li><?php echo anchor('home/lab', 'Lab');?></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="page">
    <!-- navbar-->
    <header class="header">
      <nav class="navbar">
        <div class="container-fluid">
          <div class="navbar-holder d-flex align-items-center justify-content-between">
            <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
              <div class="brand-text d-none d-md-inline-block"><span>Menu </span><strong class="text-primary">Barang</strong></div></a></div>
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Log out-->
                <li class="nav-item"><a href="<?= site_url('login/logout')?>" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>

      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="card">
              <div class="card card-primary">
                <div class="card-header d-flex align-items-center"><strong>Tambah Barang</strong></div>
                <div class="card-body">
                  <form id="formBarang" method="post" action="#">
                    <input type="hidden" id="id" value="">
                    <div class="form-group">
                      <label for="nama_lab">Nama lab</label>
                      <select class="form-control" name="id_lab" id="comboLab">
                        <!-- Generate by AJAX -->
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="tanggal">Tanggal</label>
                      <input class="form-control" type="date" name="tanggal" id="tanggal" placeholder="Tanggal">
                    </div> 
                    <div class="form-group">
                      <label class="control-label" for="nama_barang">Nama Barang</label>
                      <input class="form-control" type="text" name="nama_Barang" id="nama_barang" placeholder="Nama Barang">
                    </div> 
                    <div class="form-group">
                      <label for="status">Status Barang</label>
                      <select class="form-control" id="status" name="status">
                        <option disabled="" selected="">--pilih status barang--</option>
                        <option value="OK">OK</option>
                        <option value="NG">Not Good</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="jumlah">Jumlah</label>
                      <input class="form-control" type="number" min="1"  name="jumlah" id="jumlah" placeholder="Masukkan Jumlah barang">
                    </div> 

                    <br>
                    <button class="btn btn-success" type="submit">Simpan Data</button>
                    <button disabled="disabled" type="button" class="btn btn-warning" id="cancelBtn">Batal</button>
                  </form>

                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="card">
              <div class="card card-primary">
                <div class="card-header d-flex align-items-center">
                  <strong><center>Data Barang</center></strong>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table id="tableBarang" class="table table-hover">
                      <thead>
                        <tr>
                          <th>###</th>
                          <th>Tanggal</th>
                          <th>Nama Barang</th>
                          <th>Status Barang</th>
                          <th>Lab</th>
                          <th>Jumlah</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <p class="text-center">Copyright &copy; 2019 - <a href="https://www.facebook.com/Adiguesst" target="_blank">Adiguest</a> </p>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <!-- JavaScript files-->
    <script src="<?php echo base_url('/assets/template/vendor/jquery/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/vendor/popper.js/umd/popper.min.js');?>"> </script>
    <script src="<?php echo base_url('/assets/template/vendor/bootstrap/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/js/grasp_mobile_progress_circle-1.0.0.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/vendor/jquery.cookie/jquery.cookie.js')?>"> </script>
    <script src="<?php echo base_url('/assets/template/vendor/jquery-validation/jquery.validate.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/template/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js');?>"></script>
    <!-- Main File-->
    <script src="<?php echo base_url('/assets/template/js/front.js');?>"></script>

    <script type="text/javascript">

      let cekMethod = 'insert';
      let url = '';
      var id;

      $(document).ready(function(){

        loadData();
        loadCombo();

        $('#formBarang').on('submit', function(e){
          e.preventDefault();

          if(cekMethod == 'insert'){
            url = 'http://localhost/inven/ws/index.php/barang/add';
          }else if(cekMethod == 'update'){
            url = 'http://localhost/inven/ws/index.php/barang/update/' + id;
          }
          console.log("URL : ", url);
      //Simple Validasi
      if($('#comboLab').val() == ''){
        alert('Data lab harus diisi !');
        return;
      }else if($('#status').val() == ''){
        alert('Data status harus diisi !');
        return;
      }else if($('#nama_barang').val() == ''){
        alert('Data nama barang harus diisi !');
        return;
      }else if($('#jumlah').val() == ''){
        alert('Data jumlah harus diisi !');
        return;
      }else if($('#tanggal').val() == ''){
        alert('Data tanggal harus diisi !');
        return;
      }
      $.ajax({
        url: url,
        method: 'POST',
        dataType: 'JSON',
        data: {
          id_lab: $('#comboLab').val(),
          status: $('#status').val(),
          nama_barang: $('#nama_barang').val(),
          jumlah: $('#jumlah').val(),
          tanggal: $('#tanggal').val()
        },
        success: function(data){
          $('#comboLab').val('');
          $('#status').val('');
          $('#nama_barang').val('');
          $('#jumlah').val('');
          $('#tanggal').val('');
        },
        error: function(err){
          console.log(err);
        }
      }).done(function(){
        console.log(cekMethod);
        loadData();
        cekMethod='insert';
      });
      return false;
    });

    //ketika tombol batal diklik
    $('#cancelBtn').on('click', function(){
      cekMethod = 'insert';
      $('#comboLab').val('');
      $('#status').val('');
      $('#nama_barang').val('');
      $('#jumlah').val('');
      $('#tanggal').val('');
      $('#cancelBtn').attr('disabled', 'disabled');
      $('#comboLab').focus();
    });

  });

  //load combo box barang
  function loadCombo(){
    let output='';
    output+=`<option value="" selected="" disabled="">- Pilih lab -</option>`;
    $.ajax({
      url: 'http://localhost/inven/ws/index.php/lab/',
      method: 'GET',
      dataType: 'JSON',
    }).done(function(response){
      let lab = response;
      $.each(lab,function(index, lab){
        output += `<option value="${lab.id_lab}">${lab.nama_lab}</option>`;
      });
      $('#comboLab').html(output);
    });
  }
  
  function loadData(){
    $.ajax({
      method: 'GET',
      url: 'http://localhost/inven/ws/index.php/barang',
    }).done(function(data){
      let barangs = data;
      let output = "";
      $.each(barangs, function(index, barang){
        console.log(barang.id);
        output+= `<tr>`
        output+=  `<td>${barang.id}</td>`;
        output+=  `<td>${barang.tanggal}</td>`; 
        output+=  `<td>${barang.nama_barang}</td>`;
        output+=  `<td>${barang.status}</td>`;
        output+=  `<td>${barang.nama_lab}</td>`;
        output+=  `<td>${barang.jumlah}</td>`,
        output+=  `<td><a href="javascript:void(0)" onclick="editData(${barang.id})" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="javascript:void(0)" class="btn btn-sm btn-danger" onclick="deleteData(${barang.id})"><span class="glyphicon glyphicon-trash"></span></a></td>`
        output+= `</tr>`
      });
      $('#tableBarang > tbody').html(output);
    });
  }

    // edit barang dengan mengambil id_barang
    function editData(id_barang){
      cekMethod = 'update';
      $('#cancelBtn').removeAttr('disabled');
      $.ajax({
        url: `http://localhost/inven/ws/index.php/barang/find/${id_barang}`,
        method: "GET"
      }).done(function(response){
        id = response.id;
        $('#id').val(response.id);
        $('#comboLab').val(response.id_lab);
        $('#status').val(response.status);
        $('#nama_barang').val(response.nama_barang);
        $('#jumlah').val(response.jumlah);
        $('#tanggal').val(response.tanggal);
        $('#comboLab').focus();
      });
    }

    // hapus barang berdasarkan id_barang
    function deleteData(id_barang){
      if(confirm('Anda Yakin Akan menghapus data ini ?')){
        $.ajax({
          url: `http://localhost/inven/ws/index.php/barang/delete/${id_barang}`,
          method: 'POST'
        }).done(function(){
          setTimeout(function(){
            loadData();
          },2000);
        });
      }
    }
  </script>
</body>
</html>