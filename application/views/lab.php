<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Invent</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/bootstrap/css/bootstrap.min.css');?>">
  <!-- Font Awesome CSS-->
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/font-awesome/css/font-awesome.min.css');?>">
  <!-- Fontastic Custom icon font-->
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/fontastic.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/grasp_mobile_progress_circle-1.0.0.min.css');?>">
  <!-- Custom Scrollbar-->
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css');?>">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/style.default.css');?>" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="<?php echo base_url('/assets/template/css/custom.css');?>">
  <!-- Favicon-->
  <link rel="shortcut icon" href="<?php echo base_url('/assets/template/img/favicon.png');?>">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
      </head>
      <body>
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <div class="side-navbar-wrapper">
            <!-- Sidebar Header    -->
            <div class="sidenav-header d-flex align-items-center justify-content-center">
              <!-- User Info-->
              <div class="sidenav-header-inner text-center"><img src="<?php echo base_url('/assets/template/img/xxx.jpg');?>" alt="person" class="img-fluid rounded-circle">
                <h2 class="h5">Adiguest</h2><span>Web Developer</span>
              </div>
              <!-- Small Brand information, appears on minimized sidebar-->
              <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>U</strong><strong class="text-primary">PT</strong></a></div>
            </div>
            <!-- Sidebar Navigation Menus-->
            <div class="main-menu">
              <h5 class="sidenav-heading">Main</h5>
              <ul id="side-main-menu" class="side-menu list-unstyled">  
                <li><?php echo anchor('home', 'Beranda');?></li>
                <li><?php echo anchor('home/barang', 'Barang');?></li>
                <li class="active"><?php echo anchor('home/lab', 'Lab');?></li>
              </ul>
            </div>
          </div>
        </nav>
        <div class="page">
          <!-- navbar-->
          <header class="header">
            <nav class="navbar">
              <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                  <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
                    <div class="brand-text d-none d-md-inline-block"><span>Menu </span><strong class="text-primary">lab</strong></div></a></div>
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                      <!-- Log out-->
                      <li class="nav-item"><a href="<?= site_url('login/logout')?>" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
                    </ul>
                  </div>
                </div>
              </nav>
            </header>
            <div class="container">
              <div class="row">
                <div class="col-md-4">
                  <div class="card">
                    <div class="card card-primary">
                      <div class="card-header d-flex align-items-center">Tambah lab</div>
                      <div class="card-body">
                        <form id="formLab" method="post" action="#">
                          <input type="hidden" id="id_lab" value="">
                          <div class="form-group">
                            <label for="nama_lab">Nama Lab</label>
                            <input type="text" class="form-control" name="nama_lab" placeholder="Input Nama Lab" id="nama_lab">
                          </div>
                          <br>
                          <button class="btn btn-success" type="submit">Simpan Data</button>
                          <button disabled="disabled" type="button" class="btn btn-warning" id="cancelBtn">Batal</button>
                        </form>

                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="card">
                    <div class="card card-primary">
                      <div class="card-header d-flex align-items-center">
                        Data Lab
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table id="tableLab" class="table table-hover">
                            <thead>
                              <tr>
                                <th>###</th>
                                <th>Nama Lab</th>
                                <th>Aksi</th>
                              </tr>
                            </thead>
                            <tbody>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <footer class="main-footer">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-sm-12">
                    <p class="text-center">Copyright &copy; 2019 - <a href="https://www.facebook.com/Adiguesst" target="_blank">Adiguest</a> </p>
                  </div>
                </div>
              </div>
            </footer>
          </div>
        </div>
        <!-- JavaScript files-->
        <script src="<?php echo base_url('/assets/template/vendor/jquery/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/template/vendor/popper.js/umd/popper.min.js');?>"> </script>
        <script src="<?php echo base_url('/assets/template/vendor/bootstrap/js/bootstrap.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/template/js/grasp_mobile_progress_circle-1.0.0.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/template/vendor/jquery.cookie/jquery.cookie.js')?>"> </script>
        <script src="<?php echo base_url('/assets/template/vendor/jquery-validation/jquery.validate.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/template/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js');?>"></script>
        <!-- Main File-->
        <script src="<?php echo base_url('/assets/template/js/front.js');?>"></script>
        <script type="text/javascript">

    // variabel untuk cek kondisi save or update
    let cekMethod = 'insert';
    //variabel untuk menyimpan URL Ajax
    let url = '';
    //untuk menyimpan id saat proses edit/update
    let id;

    $(document).ready(function(){
        //Tampilkan Data Ke tabel
        loadData();

        //kondisi ketika form lab di submit
        $('#formLab').on('submit', function(e){
          e.preventDefault();
          //dapatkan nilai dari form
          let nama_lab = $('#nama_lab').val();
          //validasi sederhana
          if(nama_lab.trim() == ''){
            alert('Nama lab Harus Diisi');
            $('#nama_lab').focus();
            return;
          }

          //cek kondisi metode penyimpanan jika mode insert maka alamatkan URL ke URI add
          if(cekMethod == 'insert'){
            url = 'http://localhost/inven/ws/index.php/lab/add';
          }else if(cekMethod == 'update'){
            url = 'http://localhost/inven/ws/index.php/lab/update/' + id;
          }
          console.log(url);

          //jalankan proses ajax untuk menyimpan data
          $.ajax({
            url: url,
            method: 'POST',
            dataType: 'JSON',
            // data berbentuk object
            data: {
              nama_lab:  $('#nama_lab').val()
            },
            success: function(data){
              //jika proses ajax berhasil kosongkan form dan matikan cancel button
              $('#nama_lab').val('');
              $('#cancelBtn').attr('disabled', 'disabled');
            }
          }).done(function(){
            console.log(cekMethod);
            loadData();
            cekMethod='insert';
            // location.reload();            
          }).catch(function(err){
            throw err;
          });
          return false;
        });

        //ketika cancel button diklik kembalikan kondisi ke insert mode
        $('#cancelBtn').on('click', function(){
          cekMethod = 'insert';
          $('#nama_lab').val('');
          $('#cancelBtn').attr('disabled', 'disabled');
          $('#nama_lab').focus();
        });

      });

    //Load data tabel
    function loadData(){
      $.ajax({
        method: 'GET',
        url: 'http://localhost/inven/ws/index.php/lab',
      }).done(function(data){

        let labs = data;
        let output = "";
        $.each(labs, function(index, lab){
          output+= `<tr>`
          output+=  `<td>${lab.id_lab}</td>`;
          output+=  `<td>${lab.nama_lab}</td>`; 
          output+=  `<td><a href="javascript:void(0)" onclick="editData(${lab.id_lab})" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="javascript:void(0)" class="btn btn-danger" onclick="deleteData(${lab.id_lab})"><span class="glyphicon  glyphicon-trash"></span></a></td>`
          output+= `</tr>`
        });
        $('#tableLab > tbody').html(output);
      });
    }

    // edit data mengambil id
    function editData(id_lab){
      cekMethod = 'update';
      $('#cancelBtn').removeAttr('disabled');
      $.ajax({
        url: `http://localhost/inven/ws/index.php/lab/find/${id_lab}`,
        method: "GET"
      }).done(function(response){
        id = response.id_lab;
        $('#id_lab').val(response.id_lab);
        $('#nama_lab').val(response.nama_lab);
        $('#nama_lab').focus();
      });
    }

    // hapus data berdasar id
    function deleteData(id){
      if(confirm('Anda Yakin Akan menghapus data ini ?')){
        $.ajax({
          url: `http://localhost/inven/ws/index.php/lab/delete/${id}`,
          method: 'POST'
        }).done(function(){
          setTimeout(function(){
            loadData();
          },200);
        });
      }
    }
  </script>
</body>
</html>