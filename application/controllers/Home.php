<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->_cek_login();
	}

	private function _cek_login()
	{
		if (!isset($this->session->userdata['id_user'])) {
			redirect(base_url("/index.php/login"));
		}
	}
	function index(){
		$this->load->view('beranda');
	}
	function barang(){
		$this->load->view('barang');
	}

	function lab(){
		$this->load->view('lab');
	}
}	